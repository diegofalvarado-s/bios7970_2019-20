**BIOS7970_2019-20: Seminar in Conservation Biology: ENM applications**

Simple repository for data and code to be used in class. At the moment, it remain a repository **in progress* and 
thus it is meant to be used excusively as part of this class. 
##  

To run the code included in this repository, you can either open the source code by clicking on top of each script 
listed under *Source* tab or you can clone it to your local computer (assumming you have git installed). For a quick
overview of the power of git and its uses, please refer to the *git_presentation.md* amd *git_intro.pdf' files
````
	git clone https://bitbucket.org/diegofalvarado-s/bios7970_2019-20.git
````
##  
## 
---
** STEP 1: Getting the necessary data**

Nowadays there are multiple different sources with data amenable to be used in ENMs. For the purpose of this class,
we are going to focus on two of the most commonly used: [GBIF](https://www.gbif.org/) for species collection 
localities, and [Worldclim](http://worldclim.org/version2) for bioclimatic data.

To get the data into R, please follow the code in *gbif_data.R* and *spp_distr_mod.R
	



---