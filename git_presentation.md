# 0. General git configuration

        git config --global user.name 'xxxxx'
        git config --global user.name 'xxxxx@xxxxx'
        git config --global color.ui true

# 1. Create git repository
        # Create example directory
        mkdir ~/Desktop/git_example
        cd ~/Desktop/git_example
        
        # Option 1: create anew
        git init
        
        # Option 2: copy an existing repository   
        git clone https://diegofalvarado-s@bitbucket.org/diegofalvarado-s/eeb416_f15.git
        
        # Checking repository status    
        git status

# 2. Create an example file
        touch readme.md
        
        # Checking repository status    
        git status
        
# 3. Adding file to git repository
        # Adding one file
        git add readme.md
        git status
        
        # Adding all TRACKED files in path at once
        git add -u
        
        # Adding all TRACKED and UNTRACKED files in path at once
        git add -A
        
        git commit -m 'Initial commit'
        git status
        

# 4. Commit changes
        echo 'This is an example readme file' > readme.md
        echo 'DFA / May 28, 2016' > readme.md
        git status
        
        # Look up differences
        git diff
        
        # Submitting changes
        git add readme.md
        git status
        
        git commit -m 'Add project description'
        git status
        
        # Amend commit
        git commit -ammend
        
# 5. Modify files
        git mv readme.md ReadMe.md
        git commit 'Update file name'
        
        git rm ReadMe.md
        git commit 'Erase example file'

# 6. Simplifying your life
    # Checking project history
        git log
        git log --pretty=oneline --abbrev-commit --graph

    # Set files to ignore
        touch .gitignore
        echo '*.pyc' > .gitignore
        echo '*~' >> .gitignore
        
        git add .gitignore
        git commit -m "added .gitignore"      
        
    # Remove an "added" file
        git reset HEAD ReadMe.md
        
    # Commit all "added" files at once
        git commit -a -m 'Upload all new files'
        
    # Recover old versions
        git checkout -- README.md
        git checkout b9985c3 -- README.md
        
    # Restore repository to the version at HEAD
        git stash
        git stash pop
        
    # Explore differences in detail
        git diff b9985c3 README.md
        git diff b9985c3 1db61a1 README.md
        git diff HEAD^^^ HEAD^ README.md
        git diff HEAD~3 HEAD~1 README.md
        
    # Solve bugs
        git bisect start
        git bisect bad
        git bisect good 1db61a1

# 7. Using git remote repositories
        # create a new working directory
        mkdir ~/Desktop/bitbucket_example
        cd bitbucket_example
        
        # initialize git repository
        git init
        git remote add origin https://BaucomLab@bitbucket.org/BaucomLab/example_repo.git
        
        # submit a file
        echo -e "BaucomLab\nDiego F. Alvarado-S." >> contributors.txt
        git add contributors.txt
        git commit -m 'Initial commit with contributors'
        git push -u origin master
        
        # retrieve all remote files
            git pull origin master

# 8. Working with branches
        # create a branch
        git branch dalvarad
        git push origin dalvarad
        
        # list all available branches
        git branch
        git branch --all
        
        # switching branches
        git checkout testing_changes
        git branch
        
        # merge branches
        git checkout master
        git merge testing_changes
        